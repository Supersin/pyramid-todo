def includeme(config):
    config.add_route('home', '/')
    config.add_route('new', '/api/todo/new')
    config.add_route('toggle', '/api/todo/{id}/toggle')
