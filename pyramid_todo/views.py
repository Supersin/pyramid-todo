from pyramid.httpexceptions import exception_response
from pyramid.response import Response
from pyramid.view import view_config
from sqlalchemy.exc import DBAPIError

from pyramid_todo import controller


def _error_handler(fn):
    def wrapper(context, request):
        try:
            return fn(context, request)
        except DBAPIError:
            return exception_response(500)
        except ValueError:
            return exception_response(404)
    return wrapper


@view_config(route_name='new', request_method='POST', decorator=_error_handler)
def create_todo(request):
    todo_text = request.body
    if not todo_text:
        return exception_response(400)

    todo_id = controller.new(request.dbsession, todo_text)

    return Response(status=201, body=str(todo_id))


@view_config(route_name='toggle', request_method='POST', decorator=_error_handler)
def update_todo(request):
    todo_id = request.matchdict['id']
    try:
        todo_id = int(todo_id)
    except ValueError:
        return exception_response(400)

    controller.update(request.dbsession, todo_id)

    return Response(status=200)
