import unittest
import transaction

from pyramid import testing


class BaseTest(unittest.TestCase):
    def setUp(self):
        from pyramid_todo.models import get_tm_session
        self.config = testing.setUp(settings={
            'sqlalchemy.url': 'sqlite:///:memory:'
        })
        self.config.include('pyramid_todo.models')
        self.config.include('pyramid_todo.routes')

        session_factory = self.config.registry['dbsession_factory']
        self.session = get_tm_session(session_factory, transaction.manager)

        self.init_database()

    def init_database(self):
        from pyramid_todo.models.init_db import Base
        session_factory = self.config.registry['dbsession_factory']
        engine = session_factory.kw['bind']
        Base.metadata.create_all(engine)

    def tearDown(self):
        testing.tearDown()
        transaction.abort()


class CreateTodoTests(BaseTest):
    def _callFUT(self, request):
        from pyramid_todo.views import create_todo
        return create_todo(request)

    def test_if_itemexists(self):
        from pyramid_todo.models import Todo
        request = testing.DummyRequest(body='Hello.', dbsession=self.session)
        self._callFUT(request)
        pagecount = self.session.query(Todo).count()
        self.assertGreater(pagecount, 0)

    def test_response_code_400_on_empty_body(self):
        request = testing.DummyRequest(body='', dbsession=self.session)
        response = self._callFUT(request)
        self.assertEqual(response.code, 400)


class UpdateTodoTests(BaseTest):
    def _callFUT(self, request):
        from pyramid_todo.views import update_todo
        return update_todo(request)

    def test_response_code_400_on_invalid_id(self):
        request = testing.DummyRequest()
        request.matchdict['id'] = 'hello'
        response = self._callFUT(request)
        self.assertEqual(response.status_code, 400)

    def test_raises_value_error_on_wrong_id(self):
        request = testing.DummyRequest(dbsession=self.session)
        request.matchdict['id'] = 9
        with self.assertRaises(ValueError):
            self._callFUT(request)

    def test_response_code_200_on_mark_todo_done(self):
        text = 'Test toggle todo.'
        from pyramid_todo.models import Todo
        self.session.add(Todo(text=text))
        new_todo_id = self.session.query(Todo).filter_by(text=text).one().id

        request = testing.DummyRequest(dbsession=self.session)
        request.matchdict['id'] = new_todo_id
        response = self._callFUT(request)
        new_todo = self.session.query(Todo).filter_by(id=new_todo_id).one()

        self.assertEqual(new_todo.done, True)
        self.assertEqual(response.status_code, 200)

    def test_response_code_200_on_mark_todo_new(self):
        text = 'Test toggle todo.'
        from pyramid_todo.models import Todo
        self.session.add(Todo(text=text))
        new_todo = self.session.query(Todo).filter_by(text=text).one()
        new_todo.done = True
        new_todo_id = new_todo.id

        request = testing.DummyRequest(dbsession=self.session)
        request.matchdict['id'] = new_todo_id
        response = self._callFUT(request)
        new_todo = self.session.query(Todo).filter_by(id=new_todo_id).one()

        self.assertEqual(new_todo.done, False)
        self.assertEqual(response.status_code, 200)
