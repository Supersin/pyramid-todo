from sqlalchemy import (
    Boolean,
    Column,
    Index,
    Integer,
    VARCHAR,
)

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Todo(Base):
    __tablename__ = 'todo'
    id = Column(Integer, primary_key=True)
    text = Column(VARCHAR(100))
    done = Column(Boolean, default=False)


Index('todo_name_index', Todo.text, unique=True, mysql_length=255)
