from pyramid_todo.models.todo import Todo


def new(session, text):
    # Add a new record to the database
    new_todo = Todo(text=text)
    session.add(new_todo)
    session.flush()

    return new_todo.id


def update(session, todo_id):

    todo_item = session.query(Todo).filter_by(id=todo_id).one_or_none()
    if not todo_item:
        raise ValueError
    if todo_item.done:
        todo_item.done = False
    else:
        todo_item.done = True
